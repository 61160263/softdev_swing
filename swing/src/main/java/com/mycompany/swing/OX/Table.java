package com.mycompany.swing.OX;




public class Table {

    char[][] Table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    Player first;
    Player second;
    private Player currentPlayer;
    private Player winner;
    int lastCol;
    int lastRow;
    int countTurn = 0;

    public Table(Player first, Player second) {
        this.first = first;
        this.second = second;
        this.currentPlayer = this.first;
    }

    public char[][] getTable() {
        return Table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void switchPlayer() {
        if (this.currentPlayer == first) {
            this.currentPlayer = second;
        } else {
            this.currentPlayer = first;
        }
    }

    public boolean setRowCol(int row, int col) {
        if (this.Table[row - 1][col - 1] != '-') {
            return false;
        }
        this.Table[row - 1][col - 1] = currentPlayer.getName();
        lastCol = col - 1;
        lastRow = row - 1;
        return true;
    }

    public boolean checkRow() {
        for (int col = 0; col < this.Table[lastRow].length; col++) {
            if (this.Table[lastRow][col] != currentPlayer.getName()) {
                return false;

            }
        }
        return true;
    }

    public boolean checkCol() {
        for (int row = 0; row < this.Table.length; row++) {
            if (this.Table[row][lastCol] != currentPlayer.getName()) {
                return false;

            }
        }
        return true;
    }

    public boolean checkX1() {
        for (int i = 0; i < this.Table.length; i++) {
            if (this.Table[i][i] != currentPlayer.getName()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkX2() {
        for (int i = 0; i < this.Table.length; i++) {
            if (this.Table[i][this.Table.length - i - 1] != currentPlayer.getName()) {
                return false;
            }
        }
        return true;
    }

    public void updateStat(){
        if(this.first==this.winner){
            this.first.win();
            this.second.lose();
        }else if(this.second==this.winner){
            this.second.win();
            this.first.lose();
        }else{
            this.first.draw();
            this.second.draw();
        }
    }
    public boolean checkWin() {
        if (checkCol() || checkRow() || checkX1() || checkX2()) {
            this.winner = currentPlayer;
            updateStat();
            return true;
        }
        if (countTurn == 9) {
            updateStat();
            return true;
        }
            return false;
    }
    public Player getWinner() {
        return winner;
    }
}
